# Barritel Control Centre API

Add to your composer.json

    {
        "repositories": [
            {
                 "type": "vcs",
                 "url": "https://bitbucket.org/btxdm/controlcentre-api"
            }
        ],
        "require": {
            "barritel/controlcentre": "dev-master"
        }
    }

and composer update.

Use like

    require_once __DIR__ . '/../vendor/autoload.php'; // Include Composer's auto-load.

    use Barritel\ControlCentre\API\CCClient;
    $api = new CCClient(<your username>, <your password>, ['mode' => 'test']);
    print_r($api->whoami());
