<?php

namespace Barritel\ControlCentre\API;

# (C) K D Tart / CMF / Barritel Ltd
# version 1.4
# Aug 2017
# Requires: php curl functions, php version 5 or greater

use Barritel\ControlCentre\API\CCAPI_Error;

class CCClient {
        private $version = '1.4';
        private $default_api_version = '0.1';
        private $default_mode = 'test';
        private $serverURL_format = 'https://%s.apicontrol.co.uk/%s/%s';
        private $numeric_mode_map = array('1'=>'live', '0'=>'test');
        private $url;
        private $ua = NULL;
        private $auth;
        private $raise_error = 0;
        private $use_json = 0;
        private $client_addr = NULL;
        private $curl_ssl_ver = NULL;
        public $last_error = NULL;

        public function __construct($user, $pass, $opt = array()) {
                if (!is_array($opt)) { throw new \Exception("Options not an array", 1); }
                $mode = $this->default_mode;
                $apiver = $this->default_api_version;
                if (array_key_exists('mode',$opt)) { $mode = $opt['mode']; unset($opt['mode']); }
                if (array_key_exists('version',$opt)) { $apiver = $opt['version']; unset($opt['version']); }
                if (array_key_exists('raise_error',$opt)) {
                        if (preg_match('/^(0|1|2|full)$/i', $opt['raise_error'])) {
                                $this->raise_error = (strtolower($opt['raise_error']) === 'full' ? 2 : intval($opt['raise_error']));
                                unset($opt['raise_error']);
                        } else {
                                throw new \Exception('Bad value for raise_error: '.$opt['raise_error'], 1);
                        }
                }
                if (array_key_exists($mode, $this->numeric_mode_map)) { $mode = $this->numeric_mode_map[$mode]; }
                if (!preg_match('/^[\w-]+$/', $mode)) { throw new \Exception("Non-valid mode: $mode", 1); }
                if (function_exists('json_encode') && function_exists('json_decode')) { $this->use_json = 1; }
                if (isset($_SERVER['REMOTE_ADDR'])) {
                        $this->client_addr = $_SERVER['REMOTE_ADDR'];
                        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                                $this->client_addr .= ', '.$_SERVER['HTTP_X_FORWARDED_FOR'];
                        }
                }
                $this->url = sprintf($this->serverURL_format, $mode, $apiver, ($this->use_json ? 'json' : 'phpserialize'));
                $this->auth = array_merge($opt, array('username'=>$user, 'password'=>$pass));
                $this->ua = "php/".get_class($this)."/".$this->version;
        }

        public function __call($func, $args) {
                if ($args) {
                        if (count($args) > 1) { throw new \Exception("Too many arguments passed to $func", 2); }
                        $args = array_shift($args);
                }
                $request = array (
                        'auth' => $this->auth,
                        'func' => str_replace('_', '.', $func),
                        'args' => $args
                );
                // curl often picks the wrong SSL version by default so make that choice ourselves:
                $ssl_versions = (isset($this->curl_ssl_ver) ? $this->curl_ssl_ver : $this->curlSSLVersions());
                while ($ssl_versions) {
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_POST, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->_encode($request));
                        curl_setopt($ch, CURLOPT_URL, $this->url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
                        curl_setopt($ch, CURLOPT_USERAGENT, $this->ua);
                        if ($this->client_addr) {
                                curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Forwarded-For: '.$this->client_addr));
                        }
                        if (($this_ssl_ver = array_shift($ssl_versions)) !== NULL) {
                                curl_setopt($ch, CURLOPT_SSLVERSION, $this_ssl_ver);
                        }
                        $response = curl_exec($ch);
                        $error = curl_error($ch);
                        curl_close ($ch);
                        if (!$error) { break; }
                        if ($ssl_versions && preg_match('/SSL connect error/i', $error)) { continue; }
                        throw new \Exception("Transport error: $error", 3);
                }
                if (!isset($this->curl_ssl_ver)) {
                        $this->curl_ssl_ver = array($this_ssl_ver);
                }
                $result = $this->_decode($response);
                if ($result === false) { throw new \Exception("Cannot decode response: $response", 3); }
                if (empty($result['success'])) {
                        $this->last_error = $result;
                        if ($this->raise_error) {
                                throw new CCAPI_Error($this->raise_error > 1 ? $result : $result['error']);
                        }
                }
                return $result;
        }

        private function _encode(&$struct) {
                return ($this->use_json ? json_encode($struct) : serialize($struct));
        }

        private function _decode(&$struct) {
                return ($this->use_json ? json_decode($struct,1) : unserialize($struct));
        }

        private function curlSSLVersions() {
                $constants = get_defined_constants(1);
                if (!isset($constants['curl'])) {
                        return array(NULL);
                }
                $vals = array();
                foreach ($constants['curl'] as $name=>$val) {
                        if (strpos($name, 'CURL_SSLVERSION') === 0) {
                                $vals[] = $val;
                        }
                }
                rsort($vals, SORT_NUMERIC);
                return $vals;
        }
}
