<?php

namespace Barritel\ControlCentre\API;

# (C) K D Tart / CMF / Barritel Ltd
# version 1.4
# Aug 2017
# Requires: php curl functions, php version 5 or greater

class CCAPI_Error extends \Exception {
        public function __construct($msg) {
                parent::__construct(serialize($msg));
        }

        public function getError() {
                return unserialize($this->getMessage());
        }
}
